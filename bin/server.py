from flask import Flask, request
import json
from flask.wrappers import Response

app = Flask(__name__)
BASE_URL = '/api/'


@app.route(BASE_URL + 'get/test', methods=['GET'])
def test_get():
    param = request.args.to_dict()
    result = {
        'code': 0,
        'message': '0',
        'data': param
    }
    return Response(json.dumps(result), mimetype='application/json')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=1234)
