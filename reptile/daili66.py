import os
import sys
import time

from bs4 import BeautifulSoup
from selenium import webdriver

from reptile.service.rds_service import RdsServcie

sys_path = os.path.dirname(os.path.dirname(
    os.path.dirname(__file__)))  # 将根目录导入 才能导到 RdsService
sys.path.append(sys_path)

domain = "https://www.landchina.com/resultNotice"


def get_content(soup, browser):
    landList = []
    trs = soup.find("table", class_="table").find_all("tr", class_="trHover")
    start = 2
    for index in range(1, len(trs) + 1):
        print(len(trs))
        browser.find_element_by_xpath(
            './*//table[@class="table"]/tr['+str(start + index)+']').click()
        soup1 = BeautifulSoup(browser.page_source, 'html.parser')
        print("点击啦")
        table = soup1.find('table', class_='table')
        trs = table.find_all('tr')
        land = []
        for tr in trs[1:]:
            tds = tr.find_all('td')

            special = isSpecial(tds)

            # 第一种情况一行四列  一行两列，最常见的格式
            if len(tds) % 2 == 0 and special is False:
                for td in tds[1::2]:
                    need = td.get_text()
                    land.append(need)
            # 第二种情况 一行七列的
            if len(tds) % 7 == 0:
                for td in tds[2::2]:
                    need = td.get_text()
                    land.append(need)
            # 第三种特殊情况 跳过一行五列之后，后面一行四列都是我需要的数据
            if len(tds) % 5 == 0:
                continue
            if len(tds) % 4 == 0 and special is True:
                for td in tds:
                    need = td.get_text()
                    land.append(need)
            # 第四種
            if len(tds) % 5 == 0 and len(trs) == 14:
                need = [' ', ' ', ' ', ' ']
                land.extend(need)

        landList.append(land)
        ws = browser.window_handles
        browser.switch_to.window(ws[1])
        print(browser.current_url)
        time.sleep(5)
        browser.close()
        print(landList)
        browser.switch_to.window(ws[0])
    return land


def next(rsoup: BeautifulSoup, browser):
    page_bar = rsoup.find("div", class_="plotAreaNearPage")
    if page_bar is not None:
        btn_next = page_bar.find("button", class_="btn-next")
        disabled = page_bar.find(
            "button", class_="btn-next").get_attribute_list("disabled")[0]
        if btn_next is not None and disabled is None:
            # 点击下一页
            browser.find_element_by_xpath(
                './*//div[@class="plotAreaNearPage"]/div[@class="el-pagination is-background"]/button[@class="btn-next"]').click()
            return True
        else:
            return False
    return False


def page_limit(rsoup: BeautifulSoup, browser, news_list):
    page_bar = rsoup.find("div", class_="plotAreaNearPage")
    if page_bar is not None:
        # 先获取第一页
        news_list.extend(get_content(rsoup, browser))
        time.sleep(2)
        flag = True
        while flag:
            flag = next(rsoup, browser)
            time.sleep(4)
            soup = BeautifulSoup(browser.page_source, 'html.parser')
            values = get_content(soup, browser)
            news_list.extend(values)
    else:
        news_list.extend(get_content(rsoup, browser))
    return news_list


def open():
    option = webdriver.ChromeOptions()
    option.binary_location = "C:\\Users\\EDY\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe"
    browser = webdriver.Chrome(options=option)
    browser.get(domain)
    browser.maximize_window()
    time.sleep(2)
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    news = []
    news = page_limit(soup, browser, news)
    print(news)

    ids = []
    for new in news:
        id = RdsServcie.add_land_news(new)
        ids.append(id)


if __name__ == '__main__':
    try:
        open()
    except Exception as e:
        print(str(e))


# 判断是否是特别的那条数据
def isSpecial(tds):
    colspans = []
    for td in tds:
        if td["colspan"].isdigit():
            col = int(td["colspan"])
            colspans.append(col)

    index = 0
    for cols in colspans:
        if cols == 2:
            index = index + 1

    if(index == len(tds)):
        return True
    else:
        return False
