import sqlalchemy
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import pymysql
from sqlalchemy.orm.session import Session

pymysql.install_as_MySQLdb()

print(sqlalchemy.__version__)
Base = declarative_base()

engine = create_engine(
    "mysql://crm:abc123456@rm-wz9t2w0y538uss457uo.mysql.rds.aliyuncs.com:3306/dls_land",
    echo=False)
DBSession = sessionmaker(bind=engine)
dbSession: Session = DBSession()