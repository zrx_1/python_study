from email.policy import default
from token import TYPE_IGNORE
from xmlrpc.client import DateTime

from h11 import Data
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import VARCHAR, Integer

from model import Base


class LandReslut(Base):
    __tablename__ = 'land_result'

    id = Column(Integer, primary_key=True)
    administrativeRegion = Column(VARCHAR(255), default='')
    supervisionNumber = Column(VARCHAR(255), default='')
    entryName = Column(VARCHAR(255), default='')
    projectLocation = Column(VARCHAR(255), default='')
    area = Column(VARCHAR(255), default='')
    landSource = Column(VARCHAR(255), default='')
    landUse = Column(VARCHAR(255), default='')
    landSupplyMode = Column(VARCHAR(255), default='')
    landUseLife = Column(VARCHAR(255), default='')
    industryClassification = Column(VARCHAR(255), default='')
    landLevel = Column(VARCHAR(255), default='')
    transactionPrice = Column(VARCHAR(255), default='')
    paymentPeriodNo = Column(VARCHAR(255), default='')
    agreedPaymentDate = Column(VARCHAR(255), default='')
    agreedPaymentAmount = Column(VARCHAR(255), default='')
    remarks = Column(VARCHAR(255), default='')
    landUseRightHolder = Column(VARCHAR(255), default='')
    plotRatioMin = Column(VARCHAR(255), default='')
    plotRatioMax = Column(VARCHAR(255), default='')
    agreedDeliveryTime = Column(VARCHAR(255), default='')
    agreedCommenceTime = Column(VARCHAR(255), default='')
    agreedCompletionTime = Column(VARCHAR(255), default='')
    actualCommenceTime = Column(VARCHAR(255), default='')
    actualCompletionTime = Column(VARCHAR(255), default='')
    approvalUnit = Column(VARCHAR(255), default='')
    contractSignDate = Column(VARCHAR(255), default='')
    isDelete = Column(TYPE_IGNORE(1), default=1)
    createAt = Column(DateTime, default=Data())
    updateAt = Column(DateTime, default=Data())
