import logging
import os
from logging import handlers

formatter = logging.Formatter(
    "%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(formatter)


def genFileHandler(fileName: str):
    path = os.path.abspath(os.path.dirname(__file__)) + '/logs/'
    if not os.path.exists(path):
        os.makedirs(path)
    filePath = path + fileName + '.log'
    # fh = logging.FileHandler(filePath, mode='a', encoding='utf-8')
    fh = handlers.TimedRotatingFileHandler(filename=filePath,
                                           when='D',
                                           backupCount=10,
                                           encoding='utf-8')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    return fh


# 生成一个logger
def genLogger(filename: str, level=logging.DEBUG):
    logger = logging.getLogger(filename)
    logger.setLevel(level)
    logger.addHandler(genFileHandler(filename))
    logger.addHandler(console)
    return logger


logger: logging.Logger = genLogger('default')
indLogger: logging.Logger = genLogger('industry_collect')
indELogger: logging.Logger = genLogger('industry_error_collect')
rdsSLogger: logging.Logger = genLogger('rds_service')
detailLogger: logging.Logger = genLogger('detail')
