import json
import time
from typing import List
from urllib.parse import urlencode
import requests
from bs4 import BeautifulSoup
from selenium import webdriver


def get_ip_list():
    print("正在获取代理列表...")
    url = 'http://15574995472.v4.dailiyun.com/query.txt?key=NPX026520Z&word=&count=1&rand=false&ltime=0&norepeat=false&detail=false'
    text = requests.get(url=url).text
    ip_list = text.split("\r\n")
    ip_list.pop(len(ip_list) - 1)
    print("代理列表抓取成功.")
    return ip_list


# def get_random_ip():
#     print("正在设置随机代理...")
#     ip_list = get_ip_list()
#     proxy_list = []
#     proxyusernm = "15574995472"
#     proxypasswd = "szz5201314+-*"

#     for ip in ip_list:
#         proxyurl = "http://"+proxyusernm+":"+proxypasswd+"@"+ip
#         proxy_list.append(proxyurl)
#     proxy_ip = random.choice(proxy_list)
#     proxies = {'http': proxy_ip, 'https': proxy_ip}
#     print("代理设置成功.")
#     return proxies


def get_list_data():
    d = {
        "pageNum": 2,
        "pageSize": 20,
        "endDate": "2010-03-15 23:59:59",
        "startDate": "2010-01-01 00:00:00"
    }
    ip_list = ["101.34.214.152:8001"]  # 我写的获取 链接 中的IP 现在只取了一个
    for ip in ip_list:
        url = "https://api.landchina.com/tGdxm/result/list"  # 我的目标
        proxyaddr = "101.34.214.152:8001"  # 后台链接取出来的IP 端口
        # proxyport = 57114
        proxyusernm = "15574995472"
        proxypasswd = "szz5201314+-*"

        proxyurl = "http://"+proxyusernm+":"+proxypasswd+"@"+proxyaddr

        t1 = time.time()
        r = requests.post(url,  data=json.dumps(d), headers={
            "Content-Type": "application/json",
            "Accept": "application/json, text/plain, */*",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "zh-CN,zh;q=0.9",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"})

        t2 = time.time()
        res = json.loads(r.text)
        if res["code"] == 200:
            print(res["data"]["list"])

            addList(res["data"]["list"])
            # for list in res["data"]["list"]:
            #     list.gdGuid

        print("时间差:", (t2 - t1))


def addList(lists: List):
    landList = []
    for id in lists:

        option = webdriver.ChromeOptions()
        option.binary_location = 'C:\Program Files\Google\Chrome\Application\chrome.exe'
        browser = webdriver.Chrome(options=option)
        print(id["gdGuid"])
        print('https://www.landchina.com/landSupplyDetail?id=' +
              id["gdGuid"] + '&type=供地结果&path=0')
        #url = urlencode('https://www.landchina.com/landSupplyDetail?id=' + id["gdGuid"] +'&type=供地结果&path=0')
        browser.get('https://www.landchina.com/landSupplyDetail?id=' +
                    id["gdGuid"] + '&type=供地结果&path=0')

        
        time.sleep(5)
        browser.maximize_window()
        time.sleep(5)

        soup1 = BeautifulSoup(browser.page_source, 'html.parser')
        table = soup1.find('table', class_='table')
        trs = table.find_all('tr')

        land = []
        for tr in trs[1:]:
            print(len(trs)+"gggggggggggggggggggggggggggg")
            tds = tr.find_all('td')
            # 第一种情况一行四列  一行两列，最常见的格式
            if len(tds) % 2 == 0:
                for td in tds[1::2]:
                    need = td.get_text()
                    land.append(need)

            if len(tds) % 7 == 0:
                for td in tds[2::2]:
                    need = td.get_text()
                    land.append
            print(land)
        # allLand.append(land)
        # print(allLand)
        time.sleep(50)
        # 先关闭，再将brower给到第一个界面
        # browser.close()
        landList.append(land)
        print(landList)

        browser.close()


if __name__ == '__main__':
    try:
        get_list_data()
    except Exception as e:
        print(str(e))


# option = webdriver.ChromeOptions()
# option.binary_location = 'C:\Program Files\Google\Chrome\Application\chrome.exe'
# browser = webdriver.Chrome(options=option)
# browser.get("https://www.landchina.com/resultNotice")
# time.sleep(5)
# browser.maximize_window()
# time.sleep(5)


# ###第一步，拿到一共有多少页，计算要点击下一页多少下
# soup = BeautifulSoup(browser.page_source, 'html.parser')
# page = soup.find('ul', class_="el-pager")
# lis = page.find_all('li')
# last = lis[len(lis)-1]
# lastPage = last.get_text()
# # print(lastPage)
# # print(type(lastPage))
# pages = int(lastPage)

# allLand = []
# ##循环下一页
# for page in range(1, pages-1):

#     ##循环下一列
#     for j in range(3, 12):
#         land = []
#         browser.find_element_by_xpath( '//table[@class="table"]/tr[' + str(j) + ']' ).click()
#         time.sleep(2)
#         h2=browser.window_handles
#         browser.switch_to.window(h2[1])
#         browser.get(browser.current_url)
#         print(browser.current_url)
#         soup1= BeautifulSoup(browser.page_source, 'html.parser')
#         table = soup1.find('table', class_='table')
#         trs = table.find_all('tr')

#         for tr in trs[1:]:
#             tds = tr.find_all('td')
#             ###第一种情况一行四列  一行两列，最常见的格式
#             if len(tds)%2 == 0:
#                 for td in tds[1::2]:
#                     need = td.get_text()
#                     land.append(need)

#             if len(tds)%7 == 0:
#                 for td in tds[2::2]:
#                     need = td.get_text()
#                     land.append
#             #print(land)
#         allLand.append(land)
#         print(allLand)
#         time.sleep(50)
#         #先关闭，再将brower给到第一个界面
#         browser.close()
#         h1 = browser.window_handles
#         browser.switch_to.window(h1[0])
#         browser.get(browser.current_url)


#     print(allLand)
    # h2=browser.window_handles
    # browser.switch_to.window(h2[1])
    # browser.get(browser.current_url)
    # print(browser.current_url)

    # time.sleep(200)
    # browser.close()
