import sqlalchemy
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base
import pymysql

# import os
# import sys
# curPath = os.path.abspath(os.path.dirname(__file__))
# rootPath = os.path.split(curPath)[0]
# sys.path.append(rootPath)

pymysql.install_as_MySQLdb()

print(sqlalchemy.__version__)
Base = declarative_base()
engine = create_engine(
    "mysql://crm:abc123456@rm-wz9t2w0y538uss457uo.mysql.rds.aliyuncs.com:3306/crm",
    echo=True)
Base.metadata.create_all(engine)
print('Create table successfully!')
