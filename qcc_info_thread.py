from selenium import webdriver
from logger import detailLogger
import json
import time
from bs4 import BeautifulSoup
from service.rds_service import RdsServcie
import re
from model.wait_crawl import WAITCRAL
from rds import dbSession
from threading import Thread

domain = "https://www.qcc.com"


def qcc_login():
    post = []
    option = webdriver.ChromeOptions()
    option.binary_location = 'C:\Program Files\Google\Chrome\Application\chrome.exe'
    browser = webdriver.Chrome(options=option)
    browser.get("https://www.qcc.com/user_login")
    time.sleep(10)
    cookies = browser.get_cookies()
    for item in cookies:
        # print(item)
        post.append(item)
        cookie_str = json.dumps(post)

    # print(cookie_str)
    with open('cookie.txt', 'w+', encoding='utf-8') as f:
        f.write(cookie_str)
        f.close()
    print("cookies信息已保存到本地")
    browser.close()


# 获取下标
def get_index(rsoup: BeautifulSoup, idstr: str):
    section = rsoup.find("section", id=idstr)
    pagination = section.find("ul", class_="pagination")
    index = 0
    if pagination is not None:
        page_lis = pagination.find_all("li")
        for i in range(1, len(page_lis)):
            if page_lis[i].get_text() == '>':
                index = i + 1
                break
    return index


# 获取总条数
def get_count_page(rsoup: BeautifulSoup, idstr: str):
    section = rsoup.find("section", id=idstr)
    pagination = section.find("ul", class_="pagination")
    next_btn_text = 0
    if pagination is not None:
        index = get_index(rsoup, idstr)
        page_lis = pagination.find_all("li")
        next_btn_text = len(page_lis)
        print(next_btn_text)
        if page_lis[index - 1] is not None:
            next_btn_text = int(page_lis[index - 1].get_text().replace(
                "...", ""))
    return next_btn_text


# 分页
def get_all(rsoup: BeautifulSoup, browser, idstr: str, touzilist):
    section = rsoup.find("section", id=idstr)
    if section is not None:
        pagination = section.find("ul", class_="pagination")
        value = []
        if pagination is not None:
            touzilist.extend(get_touzilist(rsoup, idstr, value))
            index = 0
            index = get_index(rsoup, idstr)
            data = []
            while True:
                if index == 0:
                    break
                data = []
                if idstr == 'shangbiaolist':
                    browser.find_element_by_xpath(
                        '//section[@id="' + idstr +
                        '"]/div[3]/div[2]/nav/ul/li[' + str(index) +
                        ']').click()
                elif idstr == 'supplierlist':
                    browser.find_element_by_xpath(
                        '//section[@id="' + idstr +
                        '"]/div[@class="app-ntable"]/nav/ul/li[' + str(index) +
                        ']').click()
                elif idstr == 'customerlist':
                    browser.find_element_by_xpath(
                        '//section[@id="' + idstr +
                        '"]/div[@class="app-ntable"]/nav/ul/li[' + str(index) +
                        ']').click()
                else:
                    browser.find_element_by_xpath('//section[@id="' + idstr +
                                                  '"]/div[last()]/nav/ul/li[' +
                                                  str(index) + ']').click()
                time.sleep(2)
                soup = BeautifulSoup(browser.page_source, 'html.parser')
                # 重新查询  > 下标
                index = get_index(soup, idstr)
                values = get_touzilist(soup, idstr, data)
                # print(values)
                touzilist.extend(values)
        else:
            touzilist.extend(get_touzilist(rsoup, idstr, value))
    return touzilist


# 获取表格内容
def get_touzilist(rsoup: BeautifulSoup, id: str, touzilist):
    section = rsoup.find("section", id=id)
    ntable_trs = section.find("div", class_="app-ntable").find(
        "table", class_="ntable").find_all("tr")
    for i in range(1, len(ntable_trs)):
        ntable_tr_tds = ntable_trs[i].find_all("td")
        values = []
        for tr_td in ntable_tr_tds:
            # 股东信息
            cont_span = tr_td.find("span", class_="cont")
            # img_span = tr_td.find("span")
            if cont_span is not None:
                name_span_text = cont_span.find("span",
                                                class_="name").get_text()
                # 存入名称
                values.append(name_span_text)
                tags_div = cont_span.find("div", class_="tags")
                if tags_div is not None:
                    tags_div_spans = tags_div.find_all("span")
                    tags = []
                    for spans in tags_div_spans:
                        tags.append(spans.get_text().replace("\n", "").strip())
                    # 加入标签
                    tags = list(set(tags))
                    values.append(",".join(tags))
                else:
                    values.append("")
            else:
                img = tr_td.find("img")
                if img is not None:
                    img_url = img.get_attribute_list('src')[0]
                    values.append(img_url)
                else:
                    text = str(tr_td.get_text()).replace(" ", "").replace(
                        "\n", "").replace("股权结构", "").replace(">", "")
                    if text == "详情":
                        tr_td_a = tr_td.find("a")
                        if tr_td_a is not None:
                            tr_td_a_url = tr_td_a.get_attribute_list('href')[0]
                            values.append(domain + tr_td_a_url)
                    else:
                        values.append(text)
        touzilist.append(values)
    return touzilist


# 爬取主要人员
def get_main_member(companyName):

    main_member = []
    main_member = get_all(rsoup, browser, "mainmember", main_member)
    # print(main_member)
    for main in main_member:
        main.append(companyName)
        detailLogger.info(RdsServcie.add_qicc_member(main))
    print("爬取主要人员 over")


if __name__ == '__main__':
    t1 = Thread(target=func, args=("线程1", ))
    t2 = Thread(target=func, args=("线程2", ))
    t1.start()
    t2.start()
    print("主线程结束")