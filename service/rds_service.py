
from model.landReslut import LandReslut
from rds import dbSession


class RdsServcie:

    def add_land_news(news):
        obj = dbSession.query(LandReslut).filter(
            LandReslut.supervisionNumber == news[1]).first()
        addAction = False
        if obj is None:
            addAction = True
            obj = LandReslut()
        obj.administrativeRegion = news[0]
        obj.supervisionNumber = news[1]
        obj.entryName = news[2]
        obj.projectLocation = news[3]
        obj.area = news[4]
        obj.landSource = news[5]
        obj.landUse = news[6]
        obj.landSupplyMode = news[7]
        obj.landUseLife = news[8]
        obj.industryClassification = news[9]
        obj.landLevel = news[10]
        obj.transactionPrice = news[11]
        obj.paymentPeriodNo = news[12]
        obj.agreedPaymentDate = news[13]
        obj.agreedPaymentAmount = news[14]
        obj.remarks = news[15]
        obj.landUseRightHolder = news[16]
        obj.plotRatioMin = news[17]
        obj.plotRatioMax = news[18]
        obj.agreedDeliveryTime = news[19]
        obj.agreedCommenceTime = news[20]
        obj.agreedCompletionTime = news[21]
        obj.actualCommenceTime = news[22]
        obj.actualCompletionTime = news[23]
        obj.approvalUnit = news[24]
        obj.contractSignDate = news[25]
        obj.isDelete = 0

        if addAction:
            dbSession.add(obj)
        dbSession.commit()
        return obj.id
